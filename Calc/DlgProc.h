#ifndef DLG_PROC_H_H_H
#define DLG_PROC_H_H_H

INT_PTR CALLBACK DlgProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
BOOL OnInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam); 
void OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);

#endif