#ifndef ERROR_H_H
#define ERROR_H_H

BOOL IsError();
void SetErrorState(BOOL bErrorOccur);
void ShowError();

#endif