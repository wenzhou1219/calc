//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Script.rc
//
#define IDI_APPICON                     101
#define IDD_MAINDIALOG                  102
#define IDR_MENU1                       103
#define IDR_MENU                        103
#define IDB_MC                          1001
#define IDB_MR                          1002
#define IDB_MS                          1003
#define IDB_MSUB                        1004
#define IDB_MPLUS                       1005
#define IDB_BACK                        1006
#define IDB_CE                          1007
#define IDB_C                           1008
#define IDB_0                           1011
#define IDB_1                           1012
#define IDB_2                           1013
#define IDB_3                           1014
#define IDB_4                           1015
#define IDB_5                           1016
#define IDB_6                           1017
#define IDB_7                           1018
#define IDB_8                           1019
#define IDB_9                           1020
#define IDB_PLUS                        1021
#define IDB_SUB                         1022
#define IDB_MULTIPLY                    1023
#define IDB_DIVIDE                      1024
#define IDB_POS2NEG                     1025
#define IDB_SQRT                        1026
#define IDB_PERCENT                     1027
#define IDB_RECI                        1028
#define IDB_EQUAL                       1029
#define IDB_POINT                       1030
#define IDE_CURINPUT                    1031
#define IDE_HISINPUT                    1032
#define IDE_MFLAG                       1033
#define ID_40001                        40001
#define ID_40002                        40002
#define IDM_WEBSITE                     40003

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        104
#define _APS_NEXT_COMMAND_VALUE         40004
#define _APS_NEXT_CONTROL_VALUE         1031
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
