#include "CalcVariable.h"
#include <memory.h>
#include <Windows.h>

double	dNum1;
double	dNum2;
double	dTempNum;
double	dMemNum;
BOOL	bPoint;
int		iOperate;
BOOL	bCalcState;
BOOL	bFuncState;
BOOL bMReadState;
BOOL    bMState;
char	szShowText[2048];
char	szErrorMessage[256];

void InitCalcVariable()
{
	//初始化输入变量
	dNum1		= 0;
	dNum2		= 0;
	dTempNum	= 0;
	dMemNum		= 0;
	bPoint		= FALSE;
	iOperate	= -1;
	bCalcState	= FALSE;
	bFuncState	= FALSE;
	bMReadState = FALSE;
	bMState		= FALSE;
	memset(szShowText, 0, 2048);
	memset(szErrorMessage, 0, 256);
}