#ifndef CALCVARIABLE_H_H
#define CALCVARIABLE_H_H

#include <Windows.h>

extern double		dNum1;
extern double		dNum2;
extern double		dTempNum;
extern double		dMemNum;
extern BOOL		bPoint;
extern int				iOperate;
extern BOOL		bCalcState;
extern BOOL		bFuncState;
extern BOOL		bMReadState;
extern BOOL		bMState;
extern char			szShowText[2048];
extern char			szErrorMessage[256];

void InitCalcVariable();

#endif

